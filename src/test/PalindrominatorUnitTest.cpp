/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   PalindrominatorUnitTest.cpp
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Palindrominator CPPUNIT test implementation. DO NOT MODIFY THE CONTENTS OF THIS FILE!
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <sstream>
#include "PalindrominatorUnitTest.h"
#include "../main/Palindrominator.h"


CPPUNIT_TEST_SUITE_REGISTRATION(PalindrominatorUnitTest);

PalindrominatorUnitTest::PalindrominatorUnitTest() {
    /* no-op */
}

void PalindrominatorUnitTest::setUp() {
    /* no-op */
}

void PalindrominatorUnitTest::tearDown() {
    /* no-op */
}

void testItWith(std::string text, bool expected) {
    Palindrominator palindrominator;
    bool actual = palindrominator.isPalindrome(text);
    std::stringstream ss;
    ss << "Problem: I expected " << (expected ? "true" : "false") << " but the expression \""
       << text << "\" evaluated to " << (actual ? "true" : "false") << "...";
    CPPUNIT_ASSERT_EQUAL_MESSAGE(ss.str(), expected, actual);
}

void PalindrominatorUnitTest::testOddLengthPalindrome() {
    testItWith("mom", true);
}

void PalindrominatorUnitTest::testEvenLengthPalindrome() {
    testItWith("ABBA", true);
}

void PalindrominatorUnitTest::testOddLengthNonPalindrome() {
    testItWith("c++", false);
}

void PalindrominatorUnitTest::testEvenLengthNonPalindrome() {
    testItWith("Data Structure", false);
}