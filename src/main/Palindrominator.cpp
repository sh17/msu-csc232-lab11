/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Palindrominator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Shinya Honda <sh17@live.missouristate.edu>
 * @brief   Palindrominator implementation.
 */

#include "Palindrominator.h"

bool Palindrominator::isPalindrome(const std::string text) {
    std::queue<int> aQueue;
    std::stack<int> aStack;

    for (int i = 0; i < text.size(); i++) {
    	aStack.push(text.at(i));
    	aQueue.push(text.at(i));
    }
    while (!aStack.empty() && !aQueue.empty()) {
    	if (aStack.top() == aQueue.front()) {
    		aStack.pop();
    		aQueue.pop();
    	} else {
    		return false;
    	}
    }
    return true;
}