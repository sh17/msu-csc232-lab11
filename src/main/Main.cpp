/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Shinya Honda <sh17@live.missouristate.edu>
 * @brief   Entry point for this application. This is nothing more than a playground.
 */

#include <cstdlib>
#include <iostream>

#include "Palindrominator.h"

int main(int argc, char *argv[]) {
    std::cout << "Lab 11 (Main)" << std::endl;
    Palindrominator engine;

    std::cout << "mom is a palindrome? " << (engine.isPalindrome("mom") ? "yes" : "no") << std::endl;
    std::cout << "c++ is a palindrome? " << (engine.isPalindrome("c++") ? "yes" : "no") << std::endl;

    return EXIT_SUCCESS;
}
