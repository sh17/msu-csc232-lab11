# Lab 11 - Stacks & Queues in the STL

This is a rather simple lab that introduces you to the STL implementation of stacks and queues. In this lab you will use stacks and queues (as discussed in class) to determine whether a given string is a palindrome. As such, you are tasked simply with implementing the following `Palindrominator` member function:

```
/**
  * Determine whether the given string is a palindrome.
  *
  * @param text the string under interrogation
  * @return True is returned if the given text is a palindrome, false otherwise.
  */
bool isPalindrome(std::string text);
```

The `CMakeLists.txt` file provides the ability to generate three executables (add `.exe` if you're in the Cygwin environment):

1. `Lab11` - A simple program that calls upon the `Palindrominator` to assess two simple strings. It will appear to give false results until the `isPalindrome()` operation is implemented successfully.
1. `Lab11Demo` - A simple program that shows the basic usage of the STL stack and queue structures. The output of this demo is independent of any code you write and exists simply as a reference.
1. `Lab11Test` - A CPPUNIT test that will dictate your grade. Make the `Palindrominator` pass all these tests and you're on your way to a 3/3 on this lab.

## Execution and Submission

This lab, like all others, requires that you 

1. Fork this lab into your own private repo. Don't forget to verify that `professordaehn` has admin access to your fork.
1. Clone your fork.
1. Create a develop branch within which you'll do your work.
1. Commit as necessary.
1. When finished, create a pull request to merge your develop branch into your master branch. Any pull request made against my repo will be declined.
1. Once the pull request is created, provide a link to this pull request, along with listing the partners, in a Text Submission on Blackboard.

## Due Date

This lab is due by midnight on Saturday, 06 May 2017.